# Eksport av ansatte til Berg-Hansen

## Beskrivelse av Berg-Hansen-MS
Berg-Hansen er et reisebyrå som tilbyr universitetsbrukerkonto.

Berg-Hansen-MS er en mikrotjeneste (MS) som eksporterer data Berg-Hansen trenger for å opprette/oppdatere/slette universitetsbrukerkonti.  
Disse dataene angår eksisterende brukerkonti for ansatte hos Universitetet i Oslo.

Eksporten bistår Berg-Hansen i å beslutte hvem som skal ha denne typen konto.  
Mikrotjenesten hjelper også universitetets ansatte med enklere innlogging til byrået og synkronisering av personinformasjon mellom universitetet og byrået.

### Mikrotjenesten består av følgende deler:

0. Mikrotjenesten kjører kl. 18:00 hver dag
1. Data angående alle ansatte hentes fra SAP
2. Ansatte som påfølgende dag ikke lenger er markert som aktivt arbeidende filtreres vekk
3. Data for hver enkelt ansatt hentes fra Cerebrum
4. Relevante data (se nedenfor) for hver enkelt ansatt skal eksporteres, resten filtreres vekk
5. Relevant data skrives til fil på JSON format
6. Den skrevne filen overføres til Berg-Hansen

### Relevante data angående ansatte:
- Firmakode (Hardkodet)
- Ansattnummer (SAP)
- Tittel (Kjønn: F/M fra SAP, Ms./Mr. til Berg-Hansen)
- Etternavn (SAP)
- Fornavn (SAP)
- E-post (SAP)
- Mobilnummer (SAP)
- Kostnadssted (SAP)
- Login (SAP, samme som E-post)
- Betalingsform (invoice for utvalgte ansatte, kredittkort for andre, fra Cerebrum)

Manglende feltverdi - med unntak av mobilnummer - fører til at data angående den enkelte ansatte ikke overføres.  
Gjester ved UiO overføres ikke, da de er registrert som tilknyttede og ikke ansatte.

### Berg-Hansen-MS bruker følgende APIer:

    SAP:
        Ansatt (innhentning av data om ansatte)
    Cerebrum (innhentning av data om personer)

### Cerebrum
Cerebrum er et identitets- og administrasjonssystem som brukes av flere institusjoner i UH-sektoren.

## Kontaktpunkter

### BOTT-INT Utvikling
- RT-kø: bott-int@bott-samarbeidet.no

### BOTT-INT Drift
- RT-kø: kommer

### Berg-Hansen
- Irene Knudsen, kunderådgiver (websupport@berg-hansen.no)
- Helle Havrevold Hunt, forretningsutvikler (helle@berg-hansen.no)

### Cerebrum
- [Tjenestegruppe for Cerebrum](https://www.usit.uio.no/om/tjenestegrupper/cerebrum/)

### SAP
- [Seksjon for applikasjonsforvaltning](https://www.uio.no/om/organisasjon/los/ova/saf/index.html)

### Feil og endringsønsker
Feil meldes til BOTT INT Drift, som fungerer som førstelinje. Drift kan ved
behov melde feil videre til BOTT INT Utvikling.
