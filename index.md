# Funsjonelle beskrivelser

Følgende dokumenter beskriver integrasjoner og produkters funksjonelle virkemåte:

- [Berg-Hansen integrasjonen](BergHansen.md)
- [BookitLab-ms](Bookitlab.md)
- [CIM integrasjonen](CIM.md)
- [Cristin integrasjonen](Cristin.md)
- [Dokumenttjenesten](Dokumenttjenesten.md)
- [Extensor integrasjonen](extensor.md)
- [FSHO-ms](fsho.md)
- [IGD](igd.md)
- [Kursinfo integrasjonen](kursinfo.md)
- [Ompostering-ms](ompostering.md)
- [PTE (Arkivering av permisjoner fra SAP til Ephorte)](PTE.md)
- [SETRA](Setra.md)
- [ToA (Overføring av tilsetting- og arbeidskontrakt fra SAP til ePhorte)](ToA.md)
