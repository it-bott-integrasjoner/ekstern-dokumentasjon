# Extensor-MS 

## Overordnet beskrivelse om Extensor-MS 
Integrasjonen henter data om ansatte tilknyttet stillinger og orgenheter fra 
SAP og Orgreg og lager oppsumeringer i CSV-filer som blir overført til et definert sted.  

Generelt kan hele prosessen deles i følgende deler: 
- Sjekke at applikasjonen har tilgang til serveren som skal motta filer.
- Hente data om orgenheter fra Orgreg og skrive de utvalgte deler til orgreg-csv.
- Hente data om besatte stillinger og ansatte fra SAP, knytte ansatte til stillinger som de har,
og orgenheter, og skrive resultater i ansatte-csv.
- Overføre to csv-filer til definerte serveren.

Følgende API brukes: 
    
    - SAP Ansatte (data om ansatte)
    - SAP Stillinger (data om stillinger)
    - Orgreg OU (data om orgrenheter).


#### BOTT INT Utvikling
- RT-kø: bott-int@bott-samarbeidet.no
- Ukevaktordning: <lenke kommer>


#### BOTT INT Drift
- RT-kø kommer
- Ukevaktordning: <lenke kommer>


## Driftsstatus og driftsmeldinger
[Automatisk oppdatert driftsstatus fra overvåkningsløsning vil finnes her når
overvåkningsløsning er på plass. Da vil det også finnes en e-postliste man kan
melde seg på for driftsmeldinger om integrasjoner.]

## Feil og endringsønsker
Feil meldes til BOTT INT Drift, som fungerer som førstelinje. Drift kan ved
behov melde feil videre til BOTT INT Utvikling.

Endringsønsker og eventuelle nye behov meldes inn som beskrevet her.

## Data i csv-filer som blir overført

Filen med orgenhetsdata inneholder følgende felter:

|Feltnavn|Data hentes fra|
|---|---|
|organisasjons-ID| orgreg |
|organisasjonsnavn| orgreg |
|hoved-organisasjons-ID| orgreg |
|hoved-organisasjonsnavn| orgreg |

Filen med ansatt- og stillingsdata inneholder følgende felter:

|Feltnavn|Data hentes fra|
|---|---|
|ansattnr| SAP |
|ansattdato| SAP |
|sluttdato| SAP |
|fodselsnr| SAP |
|fodselsdato| SAP |
|navn| SAP |
|status| SAP |
|stillingsnavn| SAP |
|avdeling| orgreg |
|avdelingsnavn| orgreg |
|hovedniva| orgreg |
|hovedniva_navn| orgreg |
|stillingskode| SAP |
|stillingskode_navn| SAP |
|avdelingsdato| SAP |
|adresse| SAP |
|postnr| SAP |
|poststed| SAP |
