# Cristin

## Overordnet om Cristin-integrasjonen

Integrasjonen sender person-, stillings- og organisasjonsdata fra SAP til
Cristin. Det suppleres med noe data fra OrgReg og IGA (Cerebrum og
RapidIdentity).

Cristin (Current research information system in Norway) er et nasjonalt
forskningsinformasjonssystem som skal samle og tilgjengeliggjøre informasjon om
norsk forskning, og følge opp rapporteringen av vitenskapelige publikasjoner
til Kunnskapsdepartementet og Helse- og omsorgsdepartementet
(NVI-rapportering). Cristin forutsetter at enkelte data om brukerinstitusjonen
ligger i databasen før registrering gjøres. Det gjelder:

- Data om enheter (organisasjon).
- Data om personer (ansatte, eksterne stipendiater og emeriti).
- Data om tilknytningen mellom personer og enheter (stilling).

Integrasjonen begynner å kjøre hver natt klokken 00:05, overfører dataene som
en fil til Cristin hver natt klokken før 03:00, og Cristin importerer hver natt
klokken 04:00.

## Roller og kontaktpunkter (oppdatert 27.04.2022)

Cristin driftes av SIKT. Hver institusjon har en institusjonssuperbruker, som i
denne integrasjonen har rollen som system- og prosesseier. Integrasjonen som
overfører data til Cristin er utviklet av BOTT INT og driftes av BOTT INT
Drift. BOTT ØL forvaltning har overordnet ansvar for forvaltning av DFØs
tjenester som SAP, i tillegg til at det finnes lokale forvaltningsmiljøer på
hver institusjon. Ansvar for forvaltning av OrgReg og IGA er plassert i
forskjellige team på hver institusjon.

#### Institusjonssuperbrukere

- UiB: Irene Eikefjord
- UiO: Margaret Louise Fotland
- UiT: Tanja Larssen
- NTNU: ?

#### SIKT, Cristin

- Funksjonelt kontaktpunkt: Hanne Hole Young hanne.hole.young@sikt.no
- Teknisk kontaktpunkt: Kari Helene Tofsrud kari.tofsrud@sikt.no

#### BOTT INT Utvikling

- RT-kø: bott-int@bott-samarbeidet.no
-  Ukevaktordning: <lenke kommer>

#### BOTT INT Drift

- RT-kø kommer
- Ukevaktordning: <lenke kommer>

## Driftsstatus og driftsmeldinger

[Automatisk oppdatert driftsstatus fra overvåkningsløsning vil finnes her når
overvåkningsløsning er på plass. Da vil det også finnes en e-postliste man kan
melde seg på for driftsmeldinger om integrasjoner.]

## Feil og endringsønsker

Feil meldes til BOTT INT Drift, som fungerer som førstelinje. Drift kan ved
behov melde feil videre til BOTT INT Utvikling.

Endringsønsker og eventuelle nye behov meldes inn som beskrevet her.

## Sjekkliste hvis en ansatt ikke blir overført til Cristin

[Sjekkliste kommer.]

### Filnavn

Filene har navn på formatet institusjon-cristin-ÅÅÅÅMMDD.xml, for eksempel
uio-cristin-20220422.xml.

Datoen i filnavnet må være den samme som datoen filen blir importert, altså
klokken 04:00.



### Mekanisme for overføring

Mekanisme for overføring er dokumentert i [internt dokumentasjons-repo for BOTT
INT](https://git.app.uib.no/it-bott-integrasjoner/sop/-/blob/master/tjenester/Cristin.md).


## Data

Detaljert spesifikasjon av overføringsformatet finnes her: https://www.cristin.no/teknisk/xsd/institusjonsdata/

Formålet med tabellene under er å beskrive hvor integrasjonen henter data fra,
og hvordan dataen bearbeides, før filen som er spesifisert over produseres.
Filen som leveres til Cristin er inndelt i fire deler: beskrivelse,
institusjon, organisasjon og personer.

### Beskrivelse

|Feltnavn|Datakilde|Kommentar|
|---|---|---|
|Kilde|Konfigurasjon|Universitetenes akronym. UIO, UIB, UIT, NTNU. Store bokstaver i alle ledd.|
|Dato|System|Må være samme dato som i filnavn.|
|Mottaker|-|Brukes ikke.|

### Institusjon
|Feltnavn|Datakilde|Kommentar|
|---|---|---|
|institusjonsnr|Konfigurasjon|185|
|navnBokmal|Konfigurasjon|Universitetet i Oslo|
|navnEngelsk|Konfigurasjon|University of Oslo|
|akronym|Konfigurasjon|UiO|
|lokalFridaUrl|Konfigurasjon||
|lokalFridaEpost|Konfigurasjon||
|NSDKode|Konfigurasjon||

### Organisasjon

Filen som sendes til Cristin, inneholder et element ved navn "organisasjon".
Organisasjonselementet inneholder flere enhet-elementer, som igjen inneholder
følgende felter:

|Feltnavn|Datakilde|Kommentar|
|---|---|---|
|institusjonsnr|Konfigurasjon||
|avdnr|SAP, med oppslag OrgReg||
|underavdnr|SAP, med oppslag OrgReg||
|gruppenr|SAP, med oppslag OrgReg||
|institusjonrUnder|SAP, med oppslag OrgReg||
|avdnrUnder|SAP, med oppslag OrgReg||
|underavdnrUnder|SAP, med oppslag OrgReg||
|gruppenrUnder|SAP, med oppslag OrgReg||
|datoAktivFra|SAP||
|datoAktivTil|SAP||
|enhetErstattesAv||Brukes ikke.|
|navnBokmal|SAP, med oppslag OrgReg||
|navnEngelsk|SAP, med oppslag OrgReg||
|akronym|SAP, med oppslag OrgReg||
|postadresse|SAP, med oppslag OrgReg||
|postnrOgPoststed||Brukes ikke.|
|land||Brukes ikke.|
|telefonnr|SAP, med oppslag OrgReg||
|telefaxnr|SAP, med oppslag OrgReg||
|epost||Brukes ikke.|
|URLBokmal||Brukes ikke.|
|URLEngelsk||Brukes ikke.|
|NSDKode|SAP, med oppslag OrgReg||

### Personer og ansettelser

Filen som sendes til Cristin, inneholder et element ved navn "personer", dette
elementet inneholder flere person-element, som igjen inneholder følgende
felter:

|Feltnavn|Datakilde|Kommentar|
|---|---|---|
|etternavn|IGA|IGA er autorativt for visningsnavn.|
|fornavn|IGA|IGA er autorativt for visningsnavn.|
|fnrErstatter||Brukes ikke.|
|brukernavn|IGA|Cerebrum eller RapidIdentity.|
|adresseinfo||Brukes ikke.|
|telefonnr||Brukes ikke.|
|telefaxnr||Brukes ikke.|
|epost|IGA|Cerebrum eller RapidIdentity.|
|URL||Brukes ikke.|
|personligTittel||Brukes ikke.|
|ansettelse[].institusjonsnr|Konfigurasjon||
|ansettelse[].avdnr|SAP, med oppslag OrgReg||
|ansettelse[].undavdnr|SAP, med oppslag OrgReg||
|ansettelse[].gruppenr|SAP, med oppslag OrgReg||
|ansettelse[].stillingskode|SAP||
|ansettelse[].datoFra|SAP||
|ansettelse[].datoTil|SAP||
|ansettelse[].stillingsbetegnelse|SAP||
|ansettelse[].stillingsandel|Brukes ikke.|

## Forretningslogikk

### Utvalgskriterier organisasjon

Disse utvalgskriteriene er institusjonsspesifikke.

- For UiB overføres kun organisasjonsenheter på nivå 1 (rot) 2 (fakultet) og 3
  (institutt).
- UiO har med alle enheter ned til nivå 5, alle enheter under det kuttes.
- For UiT overføres alle enheter som finnes i SAP, nivå 1 (rot), 2 (fakultet),
  3 (institutt) og 4 (forskningsgruppe)

Ansatte som tilhører en enhet som ikke overføres, men som tilfredsstiller
kravene for overføring ellers, skal få sin organisasjonstilhørighet mappet opp
til første lovlige foreldrenode før de overføres til Cristin. Dette for å sikre
at organisasjonstilhørighet i Cristin er oversiktlig og forskningsrelevant.

Organisasjonsenheter hentes fra OrgReg, basert på hvilke organisasjonsenheter
som eksisterer i SAP. Følgende kriterier må oppfylles for at en
organisasjonsenhet blir overført til Cristin:

- Alle organisasjonsenheter
  - må ha feltene
    - institutt
    - avdeling
    - underavdeling
    - gruppe
    - organisasjonsnavn
  - organisasjonsenhetenes foreldreenheter må ha feltene
    - institutt
    - avdeling
    - underavdeling
    - gruppe
  - Organisasjonsstrukturen blir redusert til å inneholde (ansatte på lavere
    nivå blir flyttet opp i hierarkiet):
    - 3 øverste nivåer for UiB
    - 5 øverste nivåer for UiO
    - 4 øverste nivåer for UiT (alle nivåer i SAP)
   - Organisasjonsenhetene ekskluderes ved eksport hvis:
     - UiB:
       - Organisasjonsenhetens kortnavn er spesifisert i filen org_illegal.csv.
       - Organisasjonsenhetens navn er spesifisert i filen org_illegal.csv.
     - Andre institusjoner har ingen filtrering.

### Utvalgskriterier person

Kun ansatte som oppfyller gitte kriterier vil bli eksportert fra SAP. Ansatte
som oppfyller punkt 1. - 4. under blir overført for alle BOTT-institusjoner.
For UiO overføres også personer som oppfyller punkt 1. - 2., dersom de har gitt
sitt samtykke i brukerinfo.uio.no til å bli eksportert i Cristin.

  1. Den ansatte har en aktiv ansettelse i SAP.
  2. Den ansatte har gyldig fødselsnummer eller D-nummer i SAP.
  3. Den ansatte har en vitenskapelig stilling, altså har stillingskategori
     «Undervisnings- og forsknings personale» i SAP.
  4. Den ansattes stilling er månedslønnet¹

I tillegg til disse skal alle emeriti og gjesteforskere overføres fra Greg.

¹ At en ansatt er månedslønnet styres av medarbeiderundergruppe, MUG. Per mai
2022 er følgende kategorier brukt, merk at dette kan endre seg:

  - MUG 01 gir vanlig månedslønn.
  - MUG 03 er vanligvis CEO, som også har månedslønn. Brukes av og til for rektor.
  - MUG 12 er stipendiater.
  - MUG 13 er postdoktorer.
  - MUG 14 – brukes om månedslønnede, men veldig sjeldent.
  - MUG 30 – pensjonister på pensjonistvillkår.
  - MUG 33 – bistilling (brukes av og til).

## Planlagte utvidelser (oppdatert XX.XX.2022)

- Utvidelse for å manuelt velge hvilke enheter som skal overføres (UiO).
- Fjerne enheter som ikke har ansatte, fra importen.
- Samtykke for overføring til Cristin for tekadm, for UiT.
- Noe for UiB?
- Lage sjekkliste til ØVA: «Hvorfor er ikke denne personen i Cristin?». Legge
  ut kontaktpunkter OG sjekkliste på nettsiden.
