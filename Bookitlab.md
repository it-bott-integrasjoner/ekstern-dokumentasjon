# Bookitlab-ms

Bookitlab-MS er en mikrotjeneste som har to deler, en cronjobb og endepunkter som
kan brukes utenfor cronjobben. Den første vedlikeholder informasjon om prosjekter 
med å spørre, oppdatere og tilbakesende prosjektdata til Bookitlab-API. APIer kan brukes for å lage 
bunter av transaksjoner eller salgsordrer og å oppdatere deres status. Disse 
APIer videresender nylagte bunter og salgsordrer til Setra eller Sotra respektivt 
som håndterer alle skritt som skjer videre (validering, videreføring til Unit4). Data
om transaksjoner er organisert hierarkisk: Batch -> Voucher -> Transaksjon.

# Roller og kontaktpunkter

Kontaktpunktet til Bookitlab-ms er BOTT-INT utvikling.

## BOTT INT Utvikling

Utviklerne har egen epostkø der de regelemssige svarer på problemstillinger

- RT-kø: bott-int@bott-samarbeidet.no

## BOTT INT Drift

Drifterne har egen epostkø der de regelemssige svarer på problemstillinger

- RT-kø: bott-int-drift@usit.uio.no

## Dataflyt

### Endepunkter

"/batch": lager en bunt og sender den til Setra. Hvis Setra allerede inneholder 
en sånn bunt, blir den oppdatert, men hvis bunten har allerede blitt sendt fra Setra videre
til Unit4, da kan den ikke lenger oppdateres. Bunten som har feilet valideringen kan 
ikke behandles videre eller slettes, og må sendes på nytt som en ny bunt med korrigerte feil.

"/order/": lager et salgsordre og sender det til Setra. Hvis Sotra allerede inneholder 
et sånt salgsordre, blir det oppdatert, men hvis salgsordret har allerede blitt sendt fra Sotra videre
til Unit4, da kan det ikke lenger oppdateres. Salgsordret som har feilet valideringen kan 
ikke behandles videre eller slettes, og må sendes på nytt som et nytt ordre med korrigerte feil.

"/projects/{project_nr}": henter delprosjekter fra Bookitlab.

### Meldingshåndtering
"health_check": en stardarthandler for BOTT-INT sine mikrotjenester som rapporterer 
at mikrotjenesten er oppe.
"update_status": mottar opplysninger om statusen som en bunt/et salgsordre har fått fra 
Setra/Sotra som har videreført et tilsvarende objekt til Unit4 og kontrollert om det 
har/ikke har blitt mottatt.

## Feilvarsling og overvåkning

Feil meldes til BOTT INT Drift, som fungerer som førstelinje. Drift kan ved
behov melde feil videre til BOTT INT Utvikling.

Endringsønsker og eventuelle nye behov meldes inn som beskrevet her.
