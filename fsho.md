# FSHO-ms

FSHO-ms er tjenesten som overfører bunter med transaksjoner fra Felles Studentsystem (FS) til Sentralt Transaksjonslager ([SETRA](Setra.md)). Integrasjonen ble initielt laget for å bli kjørt 2. virkedag i måneden, men dette er ikke lenger et krav.

Tjenesten kjører som en tradisjonell jobb ved spesifiserte tidspunkter.



# Avhengigheter
|System|Formål|
|------|------|
|FS|Uthenting av bunter|
|Unit4|Henting av første åpne periode|
|SETRA|Mottak av bunter|
|Gravitee|Eksponerer FS, Unit4 og SETRAs apier|
|OKD clusteret|Tjenesten kjører i UiOs OKD cluster|

Merk dog at dette kun spesifiserer FSHO-ms sine egne avhengigheter. SETRA er f.eks avhengig av andre deler av Unit4, som vil kunne stoppe flyten til hovedbok dersom de har nedetid. Man skal derimot kunne være trygg på at overføring fra FS til SETRA går i orden så lenge systemene ovenfor fungerer.

Gravitee
# Roller og kontaktpunkter

Kontaktpunktet til Ompostering-ms er BOTT-INT utvikling. Per i dag gjelder dette også Omposteringsløsningen, men dette vil mest sansynlig endres når driften av ompostering-ms tas over av andre.

## BOTT INT Utvikling

Utviklerne har egen epostkø der de regelemssige svarer på problemstillinger

- RT-kø: bott-int@bott-samarbeidet.no

## BOTT INT Drift

Drifterne har egen epostkø der de regelemssige svarer på problemstillinger

- RT-kø: bott-int-drift@usit.uio.no

# Drift

## Konfigurasjonsmuligheter

Standard flyt for integrasjonen er å kjøre 2.virkedag i måneden. Dette kan ignoreres ved hjelp av _disregard_workday_check_. Dette er gjort for NTNU da de ønsker å kjøre hver onsdag. 

Teknisk sett betyr dette at jobben kjører alle virkedager for UiO, men blir stoppet dersom det ikke er andre virkedag. Hos NTNU kjører jobben på onsdager, med mindre det handler 
om en uke med måneders andre virkedagen, da kjører jobben på den der dagen istedenfor onsdag; kjører ikke i uker 31-33.

En av tingene som skjer ved uthenting av bunter fra FS er at man setter bunten som overført til hovedbok. Dette kan deaktiveres ved hjelp av _fs_oppdaterfs_override_. Dette er nyttig i en initiell fase der man vil unngå å måtte nullstille ting i FS dersom noe er feilkonfigurert og man må gjøre ny import.

Det er også mulig å importere en gammel bunt på nytt ved å spesifisere buntnummeret i FS i _fs_batchnumber_override_. Dette gjør det mulig å 

Som standard vil integrasjonen sjekke om det allerede er sendt en bunt fra seg selv til SETRA denne dagen og, dersom det er tilfellet, oppdatere den. Dette kan deaktiveres slik at eventuelle oppdateringer av bunten etter feilretting går til SETRA som en ny bunt med _post_as_new_batch_override_.

I tillegg finnes følgende verdier som _er_ konfigurerbare, men som i alle tilfeller så langt forblir urørt etter initielt oppsett, fordi de avhenger av universitetenes oppsett i Unit4.

|Verdi|Beskrivelse|Typisk verdi|
|-----|-----------|------------|
|client| Firmakoden til institusjonen i Unit4|UV|
|interface|Koden til avsendersystemet|FO|
|transtype|Hvilken type transaksjon dette skal sendes til Unit4 som|GL|
|currency|Valuta|NOK|

På toppen av dette kommer paremeterne som skal gjelde for dette avsendersystemet i [SETRA](Setra.md). 

NB: Tidspunkter i OKD er definert i tidssone UTC. UTC har ikke sommertid/vintertid. 


## Kjøretid

- UiO: Hver time 8-20 på ukedager. Merk dog at jobben avbryter uten å gjøre noe dersom det ikke er 2. virkedag i måneden.
- UiT: Hver onsdag 13-20.
- NTNU: Hver time fra 12 til 19, enten på månedens andre virkedag hvis den er denne uka, eller på onsdag; kjører ikke i uker 31-33.  

Merk at tidspunktene er i tidssone UTC, som medfører at tiden forskyves en time ved bytte til/fra sommertid/vintertid.

## Dataflyt

På høynivå kan flyten beskrives slik:

1. Jobben kjører ved spesifisert tidspunkt
2. Det sjekkes om det er 2.virkedag i måneden (så lenge ikke sjekken er deaktivert)
3. Bunter hentes fra FS og valideres
4. Perioder hentes fra Unit4 og første åpne periode blir funnet
5. Buntene konverteres til SETRA format
6. Bunten sendes til SETRA.


## Feilvarsling og overvåkning

Feil meldes til BOTT INT Drift, som fungerer som førstelinje. Drift kan ved
behov melde feil videre til BOTT INT Utvikling.

Endringsønsker og eventuelle nye behov meldes inn som beskrevet her.
