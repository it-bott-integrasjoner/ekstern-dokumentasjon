# Dette dokumentet beskriver en testintegrasjon.

Testintegrasjonen skjer mellom git(lab) og Vortex.

Commiten som opprettet dette dokumentet skal fjernes når
- testing er ferdig.

Test av mermaid i vortex
```mermaid
graph TD
    A[CronJob] -->|Trigger| B(FSHO app.py)
    B --> C{Skip 2nd workday check? For acc.testing}
    C -->|No| W[is 2nd workday of the month?]
    C -->|Yes| E[Load email for error reports, from setra parameters]
    W -->|Yes| E
    W -->|No| X(Exit)
    E --> P(Get first open period from ubw)
    P --> O(Override batchnumber enabled for acc.testing?)
    O --> |Yes, get batchnumber from config|FS2
    P -->|Success| S(Does setra have existing batches for today?)
    P -->|Failure| Exit
    S -->|Failure to load| X5(Exit)
    S -->|Yes| V(Does any of the batches have validation errors?)
    S -->|No| FS1(Load new batch from FS)
    V -->|No| X2(Exit. Transfer has already happened.)
    V -->|Yes| CR(Create batchnumber based on todays date)
    CR --> FS2(Load existing batch from FS)
    FS1 --> |New batch|CHECK(Is the FS batch valid?)
    FS2 --> |Existing batch|CHECK
    CHECK --> |No|X3(Exit)
    CHECK --> |Yes|CONV(Convert from FS to setra batch)
    CONV --> VAL(Check Setra batch for errors)
    VAL -->|No error| SEND{Is batch new or existing. Overridden by config: post_as_new_batch_override}
    VAL -->|Error| X4(Exit)
    SEND --> |new batch| S2(Post to setra as a new batch)
    SEND --> |existing batch| S3(Put to setra as a batch update)
    S2 --> H(Sending to Setra went ok?)
    S3 --> H
    H --> |Yes|D(Done. Success)
    H --> |No|X6(Exit)
```

