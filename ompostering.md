# Ompostering-ms

Ompostering-ms er tjenesten som overfører data mellom Omposteringsløsningen og Unit4. Dette gjelder innsending av bunter/salgsordre til SETRA, tilbakeskriving av status til Omposteringsløsnigen, og masterdataoppdatering av Omposteringsløsningen.

Merk: Dette dokumentet beskriver *ikke* Omposteringsløsningen, kun mekanismen som gjør innsending til det sentrale transaksjonslageret ([SETRA](Setra.md)) på vegne av Omposteringsløsningen, samt oppdatering av Omposteringsløsningen med masterdata.

# Roller og kontaktpunkter

Kontaktpunktet til Ompostering-ms er BOTT-INT utvikling. Per i dag gjelder dette også Omposteringsløsningen, men dette vil mest sansynlig endres når driften av ompostering-ms tas over av andre.

## BOTT INT Utvikling

Utviklerne har egen epostkø der de regelemssige svarer på problemstillinger

- RT-kø: bott-int@bott-samarbeidet.no

## BOTT INT Drift

Drifterne har egen epostkø der de regelemssige svarer på problemstillinger

- RT-kø: bott-int-drift@usit.uio.no

## Dataflyt innsending til SETRA

Tjenesten består av to komponenter:
- Ompostering-ms
- Ompostering-publisher

Når bruker av omposteringsløsningen gjør sitt arbeid i løsningen lagres dette i en database. Ompostering-publisher henter regelmessig ut det som lagres der og publiserer en melding til ompostering-ms om at noen har ompostert noe.

Ompostering-ms plukker opp denne meldingen og gjør så et api-kall til Omposteringsløsningens api der den henter ut informasjonen om bunten og sender den til SETRA. Etter en stund mottar Ompostering-ms en melding fra SETRA om at fremdiftstatus er endret for bunten, og henter dermed ny informasjon som den sender til Omposteringsløsningens api. Dette skjer fortløpende for hvert steg gjennom prosessen i SETRA. På denne måten får brukere av Omposteringsløsningen oppdatert status på bunter underveis i behandlingen.

## Dataflyt masterdata import

For at omposteringsløsningen skal fungere optimalt trenger den regelmessig oppdatering av masterdata fra Unit4. Følgende informasjon hentes i dag fra Unit4 hver natt:

- anlegg
- arbeidsordrer
- begrepsverdier
- brukere
- firma
- kontoplan
- koststeder
- perioder
- prosjekt
- konteringsregler
- avgiftskoder

## Feilvarsling og overvåkning

Feil meldes til BOTT INT Drift, som fungerer som førstelinje. Drift kan ved
behov melde feil videre til BOTT INT Utvikling.

Endringsønsker og eventuelle nye behov meldes inn som beskrevet her.
