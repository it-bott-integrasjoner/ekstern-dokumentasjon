# IGD 

Integrasjon mellom IGA og SAP.

## Overordnet beskrivelse av IGD

Integrasjonen overfører masterdata (e-postadresse, Feide-ID og telefonnumre) om ansatte fra IGA (Cerebrum, RapidIdentity) til DFØ SAP, i forbindelse med opprettelse eller endringer på masterdata som IGA er master for.

Dette gjøres slik at ledere til enhver tid har oppdatert kontaktinformasjon om ansatte, samt at Feide-innlogging til SAP blir mulig.

### Dataflyt 1: Masterdata fra Cerebrum til SAP
Cerebrum er master for e-postadresser, brukernavn og kontortelefonnummer for UiO, UiT og NTNU. Når disse oppstår eller endres i Cerebrum, skriver IGD disse til SAP innen kort tid.

### Dataflyt 2: Masterdata fra RapidIdentity til SAP
RapidIdentity er master for e-postadresser, brukernavn og enkelte telefonnumre for UiB. Når disse oppstår eller endres i RapidIdentity, skriver IGD disse til SAP innen kort tid.


## Kontaktpunkter

|System|Kontaktpunkt|
|----|----|
|Cerebrum UiO| Joakim Hovlandsvåg (joakim.hovlandsvag@usit.uio.no), Tobias Langhoff (USIT, tobias.langhoff@usit.uio.no) |
|SAP UiO| Tone Morken (tone.morken@admin.uio.no) |
|Cerebrum UiT| Kenneth Johansen (kenneth.a.johansen@uit.no) |
|SAP UiT| Merete Danielsen (merete.b.danielsen@uit.no) |
|Cerebrum NTNU | - |
|RapidIdentity UiB| Gisle Aas (gisle.aas@uib.no), Elin-Mari Bjørndal (elin.bjorndal@uib.no) |
|SAP UiB| Lyder Jensen  (lyder.jensen@uib.no) |


## Data og overføring

IGD tjenesten lytter på spesifikke meldinger fra Cerebrum og RapidIdentity (endringer og nyopprettelser). Meldingene, og feltene det skal reageres på, er forskjellig mellom de som kommer fra RapidIdentity, og Cerebrum

|Universitet|IGA system|
|---|---|
|UiO, UiT, NTNU|Cerebrum|
|UiB|RapidIdentity|


### Dataflyt: Masterdata fra Cerebrum eller RapidIdentity til SAP

Dataflyten utløses av opprettelse eller endring i masterdata i Cerebrum eller RapidIdentity.

For Cerebrum lyttes det i IGD på følgende meldinger:

For konto:

- no.uio.cerebrum.scim.accounts.create
- no.uio.cerebrum.scim.accounts.modify

For personer:

- no.uio.cerebrum.scim.persons.create
- no.uio.cerebrum.scim.persons.modify
- no.uio.cerebrum.scim.persons.join

Dersom meldingen ikke er en av følgende event typer, så reageres det ikke videre på meldingen: 

- ADD
- CREATE
- MODIFY
- ACTIVATE

Dersom den er en av de fire nevnte typer, så undersøkes det om noen av følgende datafelter er endret :


|UiO, UiT, NTNU|UiB|
|----|----|
|name|no:edu:scim:user:employeeNumber|
|email|no:edu:scim:user:eduPersonPrincipalName|
|no:edu:scim:user:userPrincipalName|no:edu:scim:user:accountType|
|phone|emails[type eq "work"]|
|accountType|phoneNumbers[type eq "work"]|
|Dfo_pid||

(merk: det er ingen sammenheng mellom feltene i de to kolonnene)


Dersom et av datafeltene fra tabellen over, er endret, hentes data om personen via riktig api, gjennom abstract-iga-client, basert på `resource_id` feltet i notifikasjonen, som er en `user_id`.

Dataene (IgaPerson) ser slik ut:

|Feltnavn|Beskrivelse|
|---|---|
|iga_person_id| iga id |
|sap_person_id| sap person id |
|fs_person_id| fs person id |
|display_name| Fornavn Etternavn |
|primary_email| E-post adresse|
|phone_numbers|Telefonnumre|
|accounts|Iga kontoer (med type, brukernavn og E-post adresse)|
|feide_id|Feide id|


Deretter brukes et subset av de dataene, til å lage et mindre dataobjekt, som bare innholder feltene vi vil sende videre som en oppdatering til sap. 

Da får vi et dataobjekt som sendes som en oppdatering (PATCH) til SAP, på `sap_person_id`, som ser slik ut:

|Feltnavn|Data hentes fra|
|---|---|
|id| sap_person_id |
|ekstern_ident| feide_id |
|telefonnummer| Første telefonnummer fra listen phone_numbers |
|epost| primary_email |


**Merk, for UiT:** For UiT overføres ikke telefonnummer

**Kriterie for utvalg:** Alle brukere i Cerebrum eller RapidIdentity som har et DFØ SAP ansattnummer.

## IGD bruker følgende apier

- SAP
- Cerebrum


IGD bruker dfo-sap-client til å aksessere SAP.

IGD bruker abstract-iga-client for å enklere aksessere IGA data fra de ulike universitetene.


### abstract-iga-client

[abstract-iga-client](https://git.app.uib.no/it-bott-integrasjoner/abstract-iga-client) er et bibliotek utviklet for å enklere aksessere IGA data fra alle universitetene, ettersom universitetene gjerne har ulike systemer for slikt.

Cerebrum og RapidIdentity

abstract-iga-client benytter seg igjen av:

- cerebrum-client
- ntnu-tia-client
- scim2-client

### Cerebrum og scim Api dokumentasjon

- Cerebrum: [https://www.usit.uio.no/om/tjenestegrupper/cerebrum/dokumentasjon/om-cerebrum/](https://www.usit.uio.no/om/tjenestegrupper/cerebrum/dokumentasjon/om-cerebrum/)
- SCIM API: [https://en.wikipedia.org/wiki/System_for_Cross-domain_Identity_Management](https://en.wikipedia.org/wiki/System_for_Cross-domain_Identity_Management)
- SCIM hos UiB: [https://git.app.uib.no/it-bott-integrasjoner/iam-scim/](https://git.app.uib.no/it-bott-integrasjoner/iam-scim/)


## Drift og Utvikling

### BOTT INT Utvikling
- RT-kø: bott-int@bott-samarbeidet.no
- Ukevaktordning: <lenke kommer>


### BOTT INT Drift
- RT-kø kommer
- Ukevaktordning: <lenke kommer>


### Driftsstatus og driftsmeldinger
[Automatisk oppdatert driftsstatus fra overvåkningsløsning vil finnes her når
overvåkningsløsning er på plass. Da vil det også finnes en e-postliste man kan
melde seg på for driftsmeldinger om integrasjoner.]

### Feil og endringsønsker
Feil meldes til BOTT INT Drift, som fungerer som førstelinje. Drift kan ved
behov melde feil videre til BOTT INT Utvikling.

Endringsønsker og eventuelle nye behov meldes inn som beskrevet her.


