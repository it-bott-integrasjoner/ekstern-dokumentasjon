# Setra

SEntralt TRAnsaksjonslager (SETRA) er det sentrale punktet for innsending av bunter og salgsordre til hovedbok i Unit4 ERP.

Tjenesten tilgjengeligjør et API i hver institusjons api-manager (Gravitee) hvor de forskjellige avsendersystemene kan sende inn sine bunter og salgsordrer. Etter mottak kjøres bunter og salgsordre gjennom hvert sitt sett med [valideringsregler](#valideringsregler). De forskjellige valideringsreglene benytter seg av masterdata fra Unit4 og et sett med [parametre](#parametre) som settes per avsendersystem i SETRA.

Dersom bunten passerer alle valideringsregler sendes den til Unit4 hvor de kjører sine egne valideringsregler før det gjøres import til hovedbok.

Siden hele denne prosessen tar en del tid gjøres den asynkront, og innsender til SETRA får kun respons om at bunten/salgsordren er mottatt i SETRA. For å gjøre det mulig å følge fremdriften gjennom prosessen publiserer SETRA meldinger når fremdriftsstatus endrer seg. Disse tilgjengeliggjøres via RabbitMQ/BROM. 

For å sikre at bunter og salgsordre fra forskjellige institusjoner ikke blandes sammen har hver institusjon sin egen instans av SETRA, og data lagres i separate databaser, vel og merke på samme vertmaskin(host). 

# Roller og kontaktpunkter

Kontaktpunktet til SETRA er BOTT-INT utvikling. Merk at dette ikke nødvendigvis er tilfellet for avsendersystemene som sender inn bunter/salgsordre til SETRA.

## BOTT INT Utvikling

Utviklerne har egen epostkø der de regelemssige svarer på problemstillinger

- RT-kø: bott-int@bott-samarbeidet.no

## BOTT INT Drift

Drift er under oppstart. I mellomtiden kan utviklerne nevnt over kontaktes.

# Dataflyt

## Avsendersystemer

Setra har støtte for så mange avsendersystemer man måtte ønske. I skrivende stund finnes det 2-5 avsendersystemer ved de forskjellige insitusjonene, men vi forventer flere på sikt.

Av disse er 3 utviklet av BOTT-INT:
 - [Ompostering-ms](ompostering.md)
 - [FSHO-ms](fsho.md)
 - [BookitLab-ms](bookitlab.md)

## Mekanisme for mottak og innsending
SETRA mottar bunter og salgsordre i sitt REST-API tilgjengeligjort i hver institusjons Gravitee. Bunter/Salgsordre sendes til Unit4s REST-API tilgjengeliggjort i samme Gravitee.

## Forretningslogikk

SETRAs forrettningslogikk består hovedsakelig av fire ting:

1. Autentisering og autorisering av innsender
2. Validering av bunt/salgsordre
3. Innsending til, og uthenting av status fra, Unit4
4. Publisering av meldinger om status underveis i prosessen.

### Tilgangsstyring

Hver applikasjon autentiserer med api-nøkkel i Gravitee, og SETRA verifiserer deretter at denne applikasjonen får lov til å sende inn bunter for det angitte avsendersystemet. Det er dermed mulig for én applikasjon å sende inn bunter/salgsordre for flere avsendersystemer.

I prinsippet er det også mulig å tillate flere applikasjoner å sende som samme avsendersystem, men dette er noe vi fraråder da det gjør revisjon av regnskapet vanskeligere, da vi ikke kan si med sikkerhet hvilken av de tillate applikasjonene som sendte inn bunten/salgsordren.

### Valideringsregler
Antallet regler gjør det uforholdsmessig å inkludere alle her. Vi henviser derfor til funksjonelt design av tjenesten. Implementasjonen i kildekoden kan ses [her](https://git.app.uib.no/it-bott-integrasjoner/setra/-/tree/master/validation)

## Parametre

Hvert avsendersystems har ett sett parametre. Dette kan utvides i fremtiden dersom behovet for å ha forskjellige parametre for salgsordre og bunter per avsendersystem dukker opp.

|Navn|Mulige verdier|Brukes til|
|----|--------------|----------|
|ATTESTANT_REFERANSE_FRA_AVSENDER|J/N|Bestemmer om avsender skal fylle ut exref|
|ATTESTANT_REFERANSE_DEFAULT|Fritekst|Standardverdi for exref dersom over er satt til J|
|ATTESTANT_REFERANSE_KONTROLL|J/N|Bestemmer om innholdet i exref skal verifiseres|
|BILAGSART_FRA_AVSENDER|J/N|Bestemmer om avsender skal fylle ut bilagsart|
|BILAGSART_DEFAULT|Fritekst|Standardverdi for bilagsart dersom over er satt til J|
|BUNT_BILAGSNR_KONTROLL|J/N|Aktiverer valideringsregel 14 for krav om løpende serie med bilagsnumre|
|FEILVARSEL_FAGLIG|epostadresse|Adresse for varsling ved problemer av faglig art|
|UBW_GL07_RAPPORT_VARIANT|500/510/?|Rapporttypen for å hente ut status fra Unit4|
|USE_LINE_TOTAL| 1/0 | Bestemmer om salgsordre skal sendes inn med useLineTotal satt til 1|
|DIM1_KOSTSTED_DEFAULT|Fritekst|Standardverdi for dim1. Settes dersom verdi ikke er satt|
|DIM2_PROSJEKT_DEFAULT|Fritekst|Som over for dim2|
|DIM3_DEFAULT|Fritekst |Som over for dim3|
|DIM4_DEFAULT|Fritekst |Som over for dim4|
|DIM5_DELPROSJEKT_DEFAULT|Fritekst |Som over for dim5|
|DIM6_ANSATTNR_ANLEGG_DEFAULT|Fritekst |Som over for dim6|
|DIM7_BYGGNR_APAKKE_DEFAULT|Fritekst |Som over for dim7|

Parametre som ikke brukes av SETRA direkte:
|Navn|Mulige verdier|Brukes til|
|----|--------------|----------|
|BUNTNR_FRA_AVSENDER|Fritekst|Ukjent|
|BILAGSNR_FRA_AVSENDER|Fritekst|Ukjent|
|FEILVARSEL_TEKNISK|epostadresse | Adresse for varsling ved problemer av teknisk art |
|STATUS_MELDING_TIL_AVSENDER|Fritekst|Ukjent|

Merk at disse parameterne eksponeres i REST-APIet til SETRA. Det kan dermed tenkes at de brukes av avsendersystemer ved at de slår opp denne verdien i SETRA.


## Fremdriftsstatus
Underveis i innsendingsprosessen gis bunten og salgsordre en fremdriftsstatus, slik at avsendersystemet kan følge livsløpet på vei til Unit4.
Prosessen for innsending av salgsordre og bunter har noen forskjeller, som gjenspeiles i hvilke fremdriftsstatuser som er mulige for de to.

### Bunter

|Status| Betydning|
|------|----------|
|CREATED| Bunten er mottatt ok i SETRA, neste steg er validering |
|VALIDATION_COMPLETED| Validering er fullført uten feil, neste steg innsending til Unit4 |
|VALIDATION_FAILED| Validering er fullført, men har oppdaget feil, stopp |
|SENT_TO_UBW| Bunten er sendt og mottatt ok hos Unit4, neste steg polling |
|SEND_TO_UBW_FAILED| Innsending til Unit4 feilet, stopp |
|POLLING_COMPLETED| Bunten er ferdig motatt i Unit4, og vi kan sjekke status. Neste steg analyse av import rapporten |
|POLLING_FAILED| Bunten finnes ikke i Unit4 selv om den ble mottatt ok, stopp |
|UBW_IMPORT_OK| Rapporten har ingen feil, import er ok, neste steg henting av endelig bilagsnummer |
|UBW_IMPORT_FAILED| Rapporten har feil, import er stoppet i Unit4, stopp |
|FETCH_FINAL_VOUCHERNO_COMPLETED| Henting og lagring av bilagsnummer gikk bra, SETRA anser bunten ferdigbehandlet |
|FETCH_FINAL_VOUCHERNO_FAILED| Henting av bilagsnummer feilet, import antas har gått bra, brør sjekkes av noen i Unit4. SETRA anser bunten ferdigbehandlet |
|UBW_DELETION_OK|Neste steg dersom sletting i Unit4 gikk bra etter UBW_IMPORT_FAILED |
|UBW_DELETION_FAILED|Neste steg dersom sletting i Unit4 ikke gikk etter UBW_IMPORT_FAILED. Full stopp|

#### Ekstra 
|Status| Betydning|
|------|----------|
|HANDLED_MANUALLY|Til bruk ved ekstraordinære situasjoner der en superbruker håndterer import til Unit4 manuelt.|

### Salgsordre
|Status| Betydning|
|------|----------|
|CREATED|Salgsordren er mottatt ok i SETRA, neste steg er validering|
|VALIDATION_COMPLETED|Validering er fullført uten feil. Neste steg er innsending til Unit4|
|VALIDATION_FAILED|Validering er fullført, men har oppdaget feil, stopp|
|SENT_TO_UBW|Salgsordren er sendt og mottatt ok hos Unit4. Neste steg er analyse av rapporten|
|SEND_TO_UBW_FAILED|Innsending til Unit4 feilet, stopp|
|UBW_IMPORT_OK|Rapporten har ingen feil, import er ok. SETRA anser bunten som ferdigbehandlet|
|UBW_IMPORT_FAILED|Rapporten har feil, import er stoppet i Unit4, stopp|

## Varsling ved feil på Bunter/salgsordre

Dersom bunten når en status som fører til full stopp og man har registrert en epostadresse i FEILVARSEL_FAGLIG parameteren for dette avsendersystemet vil det bli sendt en epost som informerer om feilen. Dersom bunten er blitt stoppet er det mulig å oppdatere den slik at den kan sendes inn på nytt. Dette gjelder hovedsakelig dersom bunten er stoppet på grunn av valideringsfeil i SETRA eller Unit4. Merk dog at UBW_DELETION_FAILED betyr at bunten hadde feil, men at Unit4 ikke lar oss slette den, som igjen betyr at noen må gjøre manuelt arbeid. Salgsordre kan kun stoppes dersom de er stoppet i SETRA.


# SOTRA
Salgsordre-biten av SETRA ble i planlegingsfasen kalt SOTRA. Man så derimot underveis at det var hensiktsmessig at salgsordre levd som en komponent i SETRA. Dersom du leser informasjon om SOTRA kan du regne med at det er snakk om salgsordre-biten av SETRA.
