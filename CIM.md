# CIM-eksport

## Overordnet om CIM-integrasjonen

Cronjobben, som kjører i UiO sitt miljø, henter data fra SAP og IGA for å lage et organisasjonstre for å
videreføre det til CIM-endepunktet. I UiB sitt miljø lager prosessen et organisasjonstre for ansatte og
et tre for studenter.

Cronjobben begynner hver dag:
- UiO: kl. 04:17
- UiB: kl. 03:30

## Roller og kontaktpunkter

CIM-ms, mikrotjenesten som overfører data til CIM er utviklet av BOTT INT og 
driftes av BOTT-INT Drift.

#### BOTT INT Utvikling

- RT-kø: bott-int@bott-samarbeidet.no
- Ukevaktordning: <lenke kommer>

#### BOTT INT Drift

- RT-kø kommer
- Ukevaktordning: <lenke kommer>

## Feil og endringsønsker

Feil meldes til BOTT INT Drift, som fungerer som førstelinje. Drift kan ved
behov melde feil videre til BOTT INT Utvikling.

Endringsønsker og eventuelle nye behov meldes inn som beskrevet her.

---
Delen nedenfor gjelder cronjobben i UiB sitt miljø.

### Filnavn

Det finnes 4 filer for hver dato og de har navn på formatet:
ÅÅÅÅ-MM-DD-employees.json
ÅÅÅÅ-MM-DD-employee_orgs.json
ÅÅÅÅ-MM-DD-student_orgs.json
ÅÅÅÅ-MM-DD-next_of_kin.json

## Filer og data

### employees.json

|Feltnavn|Pliktig|
|---|---|
|username|Ja|
|employee_id|Ja|
|firstname|Ja|
|lastname|Ja|
|org_import_id|Ja|
|email|Ja|
|job_title|Ja|

### employee_orgs.json
|Feltnavn|Pliktig|
|---|---|
|name|Ja|
|key|Ja|
|parent_key|Nei|

### student_orgs.json
|Feltnavn|Pliktig|
|---|---|
|name|Ja|
|key|Ja|
|parent_key|Nei|

### next_of_kin.json
|Feltnavn|Pliktig|
|---|---|
|id|Ja|
|firstname|Ja|
|lastname|Ja|
|phone|Ja|
|relation|Ja|
|relation_to|Ja|
