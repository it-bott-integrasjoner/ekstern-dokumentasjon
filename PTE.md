# Arkivering av permisjoner fra SAP til Ephorte

## Beskrivelse av PTE-MS

Permisjoner til Ephorte er en mikrotjeneste som henter ferdigprosseserte permisjonssøknader fra SAP og arkiverer disse i arkivsystemet Ephorte.

### Mikrotjenesten består av følgende deler:

0. Mikrotjenesten kjører en gang i døgnet
1. Permisjonssøknader og tilhørende eskjemalogger hentes fra SAP
2. Søknader som ikke er ferdigbehandlet filtreres vekk
3. Søknader som ble ferdigbehandlet for lengre enn 28 dager siden filtreres vekk
4. Tjenesten søker i ePhorte på søknader, og gjenarkiverer ikke dem som allerede er arkivert
5. Data angående søknader til arkivering forsøkes sendt til Ephorte (se nedenfor for beskrivelse av hvilke datafelter dette gjelder)
6. Arkiveringsforsøk som lykkes og forsøk som feiler blir nedskrevet - med eventuell feilårsak - i et rapportdokument på PDF-format
7. Rapporten sendes som epost til tjenestens interessenter

### Følgende SAP API brukes:

    - Ansatt (data angående ansatte)
    - Ansattepermisjoner (data angående permisjonssøknader)
    - Eskjemalogg (data angående behandlingsprosessen av permisjonssøknader)

### Datafelter

#### Sak

| Feltnavn         | Kilde             | Beskrivelse                                                                                                                  |
| ---------------- | ----------------- | ---------------------------------------------------------------------------------------------------------------------------- |
| AnsvarligEnhet   | Ephorte           | Organisasjonen den ansatte tilhører                                                                                          |
| ArkivdelId       | Konfigurasjon     |
| Hjemmel          | Konfigurasjon     | Hjemmel for permisjon. Avhengig av permisjonstype                                                                            |
| TilgangskodeId   | Konfigurasjon     | Avhengig av permisjonstype                                                                                                   |
| TilgangsgruppeId | Konfigurasjon     | Avhengig av permisjonstype                                                                                                   |
| Tittel           | SAP               | "{ansatt.etternavn}, {ansatt.fornavn} - Permisjon". Eksempel: "Nordmann, Per Ola - Permisjon"                                |
| TittelOffentlig  | SAP               | "{sladdet ansatt.etternavn}, {sladdet ansatt.fornavn} - Permisjon". Eksempel: "\*\*\*\*\* \*\*\*\*\* \*\*\*\*\* - Permisjon" |
| SaksstatusId     | Settes av Ephorte |

#### Journalpost

| Feltnavn            | Kilde         | Verdi                                                                                                                                                                            |
| ------------------- | ------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Administrativenhet  | SAP           | Organisasjonen den ansatte tilhører                                                                                                                                              |
| Dokumentdato        | SAP           | Dato for godkjenning av søknad                                                                                                                                                   |
| Innholdsbeskrivelse | SAP           | "Søknad om permisjon med leders beslutning - {permisjonsbeskrivelse} ({permisjonstype})". Eksempel: "Søknad om permisjon med leders beslutning - Perm v/fødsel/adopsj 80% (415)" |
| DokumenttypeId      | Konfigurasjon |                                                                                                                                                                                  |
| DokumentkategoriId  | Konfigurasjon |                                                                                                                                                                                  |
| Hjemmel             | Konfigurasjon | Hjemmel for permisjon. Avhengig av permisjonstype                                                                                                                                |
| TilgangskodeId      | Konfigurasjon | Avhengig av permisjonstype                                                                                                                                                       |
| JournalstatusId     | Konfigurasjon |                                                                                                                                                                                  |
| Tilleggsattributt2  | SAP           | Sekvensnummer for søknaden/loggen. Fungerer som ID                                                                                                                               |
| Sak                 | Ephorte       | Referanse til sakobjektet journalposten er forbundet med                                                                                                                         |
|                     |               |                                                                                                                                                                                  |

#### Permisjonssøknad

Hoveddokumentet som arkiveres har tittel "Permisjon" og filnavn "Permisjon.txt".  
Den inneholder en formatert versjon av søknaden med følgende felter.

| Feltnavn                  | Verdi                           |
| ------------------------- | ------------------------------- |
| id                        | Permisjonssøkerens ansattnummer |
| sekvensnr                 | Fungerende id                   |
| permisjonstype            | Nummerkode                      |
| fullstendig_fravarende    |                                 |
| prosent_arbeidsfor        |                                 |
| beskrivelse               | Permisjonsgrunn                 |
| startdato                 |                                 |
| sluttdato                 |                                 |
| type_saksbehandling       |                                 |
| type_saksbehandling_tekst |                                 |
| type_saksb_arsak          |                                 |
| type_saksb_arsak_tekst    |                                 |
| vedlegg                   |                                 |
| forste_fravarsdag         |                                 |
| avregningsdager           |                                 |
| avregningstimer           |                                 |
|                           |                                 |

#### Eskjemalogg

Søknadens behandlingsforløp blir arkivert som tilleggsdokument med følgende formaterte felter.

| Feltnavn           | verdi                                  |
| ------------------ | -------------------------------------- |
| sekvensnr          | Permisjonens id                        |
| ansatt_id          |                                        |
| opprettet_dato     |                                        |
| skjematype         |                                        |
| skjema_beskrivelse |                                        |
| status             | Hvor i behandlingsløpet permisjonen er |
| logg_liste         | Se oversikt under                      |
|                    |                                        |

Feltet logg_liste er en liste med følgende felter.

| Feltnavn       | Verdi              |
| -------------- | ------------------ |
| linjenr        |                    |
| type_logglinje |                    |
| dato           |                    |
| skjema_status  |                    |
| klokkeslett    |                    |
| brukernavn     | Hvem som behandler |
| prosesstype    |                    |
| meldingstekst  |                    |
|                |                    |

#### Vedlegg

Eventuelle vedlegg til permisjonen hentes fra eskjemaloggen og arkiveres som tilleggsdokument(er).  
Dette er typisk dokumentasjon på permisjonsårsak.

## Kontaktpunkter

### BOTT-INT Drift

- RT-kø: bott-int-drift@bott-samarbeidet.no

### BOTT-INT Utvikling

- RT-kø: bott-int@bott-samarbeidet.no

### Feil og endringsønsker

Feil og endringsønsker meldes til BOTT INT Drift, som fungerer som førstelinje.  
Drift melder videre til BOTT INT Utvikling dersom det er hensiktsmessig.

## Prosesseiere

- UiB: Per Christian Gaustad
- UiO: Anne-Gro Berg
- UiT: Lill Heidi Steen
- NTNU: Geir Ekle
