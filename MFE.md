# Manntallsrapport for Evalg

## Beskrivelse av MFE-MS

MFE-MS er en mikrotjeneste som henter informasjon om ansatte og deres stillinger, inkl. tilleggstillinger, fra SAP, og knytter dem sammen i en felles datastruktur.
Datastrukturen blir lagret i en cache og oppdatert en gang per døgn.

### Følgende SAP API brukes:

    - ansatte (data angående ansatte);
    - stillinger (data angående besatte stillinger);
    - orgenheter (data angående orgenheter i organisasjonsstruktur som inneholder aktive stillinger);
    - terminovervåkning (data angående datoer av tidsbegrensede avtaler).

### Datafelter

Et "Entry" er en enhet av en ansatt med tilknyttet stilling eller tilleggsstilling.
Den inneholder følgende felter:

| Feltnavn                   | Kilde                 |
| -------------------------- | --------------------- |
| ansattnr                   | SAP Ansatte           |
| navn                       | SAP Ansatte           |
| startdato                  | SAP Stillinger        |
| sluttdato                  | SAP Stillinger        |
| organisasjonsid            | SAP Stillinger        |
| kostnadssted               | SAP Orgenheter        |
| orgkortnavn                | SAP Orgenheter        |
| medarbeidegruppe           | SAP Ansatte           |
| medarbeiderundergruppe     | SAP Ansatte           |
| stilling_id                | SAP Stillinger        |
| stillingskode              | SAP Stillinger        |
| stillingstittel            | SAP Stillinger        |
| dellonnsprosent            | SAP Stillinger        |
| feide_id                   | SAP Ansatte           |
| fnr                        | SAP Ansatte           |
| stillingskategori          | SAP Stillinger        |
| type_tilleggsstilling      | SAP Stillinger        |
| avtaledato                 | SAP Stillinger        |
| kommentar                  | SAP Terminovervåkning |
| personlig_tittel           | SAP Stillinger        |

I tillegg brukes følgende felter av UiT:

| Feltnavn                   | Kilde                 |
| -------------------------- | --------------------- |
| fornavn                    | SAP Ansatte           |
| etternavn                  | SAP Ansatte           |
| hovedarbeidsforhold        | SAP Stillinger        |
| adresse                    | SAP Ansatte           |
| poststed                   | SAP Ansatte           |
| postnr                     | SAP Ansatte           |
| campusLokasjon             | SAP Ansatte           |
| kjonn                      | SAP Ansatte           |
| fodselsdato                | SAP Ansatte           |


## Kontaktpunkter

### BOTT-INT Drift

- RT-kø: bott-int-drift@bott-samarbeidet.no

### BOTT-INT Utvikling

- RT-kø: bott-int@bott-samarbeidet.no

### Feil og endringsønsker

Feil og endringsønsker meldes til BOTT INT Drift, som fungerer som førstelinje.  
Drift melder videre til BOTT INT Utvikling dersom det er hensiktsmessig.
