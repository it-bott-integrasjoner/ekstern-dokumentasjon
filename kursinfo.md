# Kursinfo

## Overordnet om Kursinfo-integrasjonen

Kursinfo-integrasjonen oppdaterer DFØ-SAP med informasjon om kurs tatt av ansatte. Data som overføres er typisk kurs i brannvern, bruk av gass, strålekilder osv. Kursinformasjonen hentes fra Canvas, FS, og csv-filer, og suppleres med data fra IGA (Cerebrum og RapidIdentity).

*Merk:* Per nå er overføring av kursinformasjon fra FS stoppet på grunn av feil i FS api.

## Roller og kontaktpunkter (oppdatert 01.09.2022)

Integrasjonen som oppdaterer DFØ-SAP med kursinformasjon er utviklet av BOTT-INT og driftes av BOTT-INT Drift. *BOTT ØL forvaltning (nytt forvaltningsorgan kommer)* har overordnet ansvar for forvaltning av DFØs tjenester som SAP, i tillegg til at det finnes lokale forvaltningsmiljøer på hver institusjon. Ansvar for forvaltning av FS, Canvas og IGA er plassert i forskjellige team på hver institusjon.

#### Kontaktpersoner ved institusjonene

- UiB: Ana Pino (Ana.Pino@uib.no)
- UiO: Raymond Qian (raymond.qian@usit.uio.no)

#### BOTT-INT Utvikling 

- RT-kø: bott-int@bott-samarbeidet.no
- Ukevaktordning: *lenke kommer*

#### BOTT-INT Drift

- RT-kø: *kommer*
- Ukevaktordning: *lenke kommer*

## Driftsstatus og driftsmeldinger

*Automatisk oppdatert driftsstatus fra overvåkningsløsning vil finnes her når*
*overvåkningsløsning er på plass. Da vil det også finnes en e-postliste man kan*
*melde seg på for driftsmeldinger om integrasjoner.*

## Feil og endringsønsker

Feil meldes til BOTT-INT Drift, som fungerer som førstelinje. Drift kan ved
behov melde feil videre til BOTT-INT Utvikling.

Endringsønsker og eventuelle nye behov meldes inn som beskrevet her *(beskrivelse kommer)*.

#### Merk:

- Hvis et kurs ikke blir overført til DFØ-SAP kan det være at den ansatte mangler i DFØ-SAP.

## Beskrivelse og data

Integrasjonen sjekker om kursoppføringene allerede finnes i DFØ-SAP, hvis de ikke finnes legges de inn der.

| Institusjon | Beskrivelse |
|----|---|
| UiO | Data fra csv-fil: <BR>- Fil lastes opp via web app. <BR>- Har Feide integrasjon. |
| UiB | Data fra Canvas: <BR>- Alle kursfullføringer hentes fra Canvas og lastes opp til DFØ-SAP. <BR>- Kjøres av en cronjob hver natt kl. 03:30. |
| UiB | Data fra FS: <BR>- Meldingsbasert. <BR>- Stoppet p.g.a. feil i FS api som fører til at meldingene ikke blir håndtert. |

### Csv-fil

- Navnet på fila må slutte på .csv og fila må være lagret med utf-8 tegnsett
- En rad per kursfullføring, med semikolon mellom hvert felt i radene
- Datoformat: DD.MM.ÅÅÅÅ

| En rad skal bestå av følgende felt, <BR>i oppgitte rekkefølge |
|----|
| Brukernavn |
| Kurskode (25 tegn) |
| Kursnavn (80 tegn) |
| Startdato |
| Sluttdato (valgfri) |

| Eksempel på rader |
|----|
| karinord;uf-1;utflukt;10.04.2020;24.10.2020 |
| olanord;br-1;brannvern;15.04.2020;10.12.2020 |
| johndoe;ma-1;måling;02.03.2020 |

### Data som lagres i SAP:

| Feltnavn | Eksempel |
|----|---|
| id | 00102989 |
| brukerident | None |
| type | K |
| kursbetegnelse | UiO-HMS100-1 |
| spesifisertUtd | Systematisk HMS-arbeid. <BR>HMS grunnkurs modul 1 |
| gyldigFraDato | 2021-01-21 |
| gyldigTilDato | 9999-12-31 |
| endretDato |  2021-05-04 |
| endretAv | X-RESTUSER |
