# Overføring av tilsetting- og arbeidskontrakt fra SAP til ePhorte

## Overordnet beskrivelse om TOA-MS 
Integrasjonen henter metadata fra SAP og arkiverer ansatte sine kontrakter i Ephorte.  
Når det blir postert / lagret nye kontrakter i TOA / SAP vil det komme meldinger i køen (ansatt_id). 
Da blir meta data og selvet kontrakten hentet fra SAP. 
Dersom kontrakten er signert bli kontrakten arkivert i Ephorte.

Generelt kan hele prosessen deles i følgende deler: 
- Hente av metadata 
- Sjekke om kontrakten er lagret fra før, dersom det er nytt kontrakt vil det bli prosessert videre.
- Hente Ephorte sin administrative enhet (via orgreg)
- Søke og lagre kontrakten i Ephorte

Løsningen vil arkivere signerte kontrakter for 

    - Timelønnede.
    - Månedslønn uten rekruttering.
    - Oppdragskontrakter.
Følgende SAP API brukes: 
    
    - Ansatt (data om ansatte)
    - Ansattelsesinfokontrakter (meta data ang kontrakt)
    - InfokontrakterFiler (data ang selvet kontrakten, pdf fil) 

#### ePhorte
Ephorte er et sak og arkivsystem fra Sikri (tidligere Evry). 
Ephorte driftes av institusjonene (noen har support avtale med Sikri)
De fleste universitetene har ePhorte v.5, men ikke samme underversjonene. 
UiB har oppgradert til ePhorte 6.0 vinteren 2020/2021. NTNU og UiO har ePhorte 5.6. 
Det er ikke store avvik knyttet til EiS i disse ulike versjonene 5.6 og 6.0 

#### Institusjonssuperbrukere
- UiB: Per Christian Gaustad
- UiO: Anne-Gro Berg
- UiT: Lill Heidi Steen, Kai Bjønenak
- NTNU: Geir E. W. Ekle, Steinar Kleven


#### BOTT INT Utvikling
- RT-kø: bott-int@bott-samarbeidet.no
- Ukevaktordning: <lenke kommer>


#### BOTT INT Drift
- RT-kø kommer
- Ukevaktordning: <lenke kommer>


## Driftsstatus og driftsmeldinger
[Automatisk oppdatert driftsstatus fra overvåkningsløsning vil finnes her når
overvåkningsløsning er på plass. Da vil det også finnes en e-postliste man kan
melde seg på for driftsmeldinger om integrasjoner.]

## Feil og endringsønsker
Feil meldes til BOTT INT Drift, som fungerer som førstelinje. Drift kan ved
behov melde feil videre til BOTT INT Utvikling.

Endringsønsker og eventuelle nye behov meldes inn som beskrevet her.

## Sjekkliste dersom arkivrapport eposten ikke kommer frem
1. Sjekke i epost kontoen (noen ganger kan det hende at eposten kommer i søppelkatalogen)
2. Se etter tidspunktene når arkivrapport jobben blir kjørt via Helm chart (ulike institusjon har ulike kjøretid)
3. Gå gjennom loggen i kibana og se etter feil i samme tidsrommene
4. Sjekk i Sentry 

## Sjekkliste for å kjøre akrivrapport med ønsket intervall
1. Logg inn til okd clusteret okd-blue-master.uio.no:8443
2. Via "cluster console" (fra dropdown meny øverst til venstre)
3. Endre cron yaml med å angi start-dato og antall dager tidligere, f.eks 28.02.2022, 28 blir det kjørt for følgende intervall [31.01.2022 - 28.02.2022] 
4. Endre tidspunktet når cron jobben skal kjøres slik at den starter

## Data
Data som utveksles fra TOA-MS til Ephorte er beskrevet i detaljer i eget dokument her [5]

Ephorte konfigurasjon er ulike pr institusjon. Referers til Helm chartet for best oversikt:  
UiO[2], UiB[3], UiT[4]  

### Beskrivelse

#### Arkivering av kontrakter
Når det kommer meldinger i køen, startes prosessen for å kunne lagre kontrakten i Ephorte
Underveis er det behov for å hente ephorte sitt administrativ id basert på kontraktens tilhørighet (organisasjons_id). 
OrgReg blir brukt for dette, videre blir eph_client brukt for å lagre ephorte sak, jornalpost og klasseringer. 
Når saken er opprettet blir også saken tildelt til en saksbehandler og avskrevet. 
Saksbehandleren settes til den integrasjon brukeren i EIS. 

Før oppretting av nytt sak blir det utført søk for å finne om det er allerede opprettet sak med en gitt tittel. 
Dersom det finnes en sak, og der blir det opprettet nytt journalpost med tilhørende kontrakt. Signerte kontraktene blir lagret som PDF fil.  
For å se signaturene er det nødvendig å åpne dokumentet i Acrobat reader, kan lese her for mer info. 

##### Gjenbruk av tidligere sak
Det er noen tilfeller spesielt hos UiB, det opprettes sak når ansatt_nr (SAP) ikke er opprettet. I slike tilfeller kommer det 
et felt i ansatt api som angir hvilken sak man skal bruke., Dersom feltet har verdi blir det utført søk for å finne denne saken 
og saken blir brukt for å opprette nytt journalposter og klasseringer.

##### Gjenbruke personal sak
Ved UiT, brukes det kun en personal sak pr ansatt, dvs ingen ulike saker for ulike kontrakter som andre institusjoner. 
UiT har derfor en annerledes måte å bygge sak tittel, hvor blant annet sak tittelen inneholder fødselsdato. 
Personal saken blir brukt for opprette jornalpost og klasseringer. 

#### Arkivrapport
Det kjøres en cron jobb annen hver uke for å forsikre at alle kontraktene som blir postert, blir arkivet i Ephorte. 
Jobben er satt om til å hente alle kontraktene som er opprettet de siste 28 dager, om noen av disse mangler i Ephorte blir det 
prøvd på å akrivere. Dersom noen av kontraktene ikke er mulig å lagre (pga feil i API'et) blir det vist pdf rapporten. 
Kontraktene som er markert med rødt er det behov for å behandle manuelt (dvs integrasjonsansvarlige, trenger å hente kontrakten i 
selvbetjeneingsportalen og arkivere). 

#### Historiske kontrakter
Dersom det er behov for å arkivere historiske kontrakter [1] kan jobben kjøres med ønsket konfigurasjon om antall dager det er ønske om 
å gå tilbake til. Det blir da produsert en akrivrapport for hver 28 dager. Dersom en kjører for siste 365 dager blir det da produsert ca 12 rapporter.

### Institusjon
De fleste institusjonene har samme oppsett, men det kan hende det er avvik på: 
ephorte versjon (UiB: v6.0, UiT, UiO og NTNU)


### Data

#### Sak
| Feltnavn                                                                        | Kilde                                         | Verdi                                                                                             | Feltnavn  i ephorte API | Kommentar                                                                                                                                         |
|---------------------------------------------------------------------------------|-----------------------------------------------|---------------------------------------------------------------------------------------------------|-------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
| Arkivdel                                                                        | Konfigurasjon  i  mikrotjeneste               | UiB: ‘PERSONAL’ UiO: ‘BILAG-DFØ’                                                                  |                         |                                                                                                                                                   |
| Hjemmel                                                                         | Konfigurasjon  i  mikrotjeneste               | UiO: ‘offl § 13 jf  fvl § 13’                                                                     |                         |                                                                                                                                                   |
| Tilgangskode                                                                    | Konfigurasjon  i  mikrotjeneste               | UiB: ‘P’ UiO: ‘PB’                                                                                |                         |                                                                                                                                                   |
| TilgangskodeId                                                                  |                                               |                                                                                                   |                         |                                                                                                                                                   |
| Primær-ordningsprinsipp                                                         | Konfigurasjon  i  mikrotjeneste               | UiB: ‘ANR’ UiO: ‘ARKNOK’                                                                          |                         |                                                                                                                                                   |
| Klassering med primærordningsprinsipp id                                        | UiB: Ansattnummer UiO: Arkivnøkkel            |                                                                                                   |                         |                                                                                                                                                   |
| Sekundær-ordningsprinsipp                                                       | Konfigurasjon  i  mikrotjeneste               | UiB: ‘ARKNOK’ UiO: ‘ANR’ UiO: Ansattnummer                                                        |                         |                                                                                                                                                   |
| Klassering med sekundær  prinsipp id                                            | UiB: Arkivnøkkel                              |                                                                                                   |                         |                                                                                                                                                   |
| Tilgangsgruppe                                                                  | Konfigurasjon  i  mikrotjeneste               | UiB: ‘14909’                                                                                      | TilgsangsgruppeId       | Ikke i bruk for UiO                                                                                                                               |
| Status                                                                          | Konfigurasjon  i  mikrotjeneste               | UiB: ‘B’ UiO: ‘B’                                                                                 |                         |                                                                                                                                                   |
| StatusId                                                                        | B (under behandling) - kontrollert av arkivet |                                                                                                   |                         |                                                                                                                                                   |
| AnsvarligEnhet                                                                  | Ephorte                                       | AdministrativtEnhet(id, journalenhetId)                                                           | AnsvarligEnhet          | Saken blir  registrert for denne  administrativt  enhet, blir  funnet  ved å gjøre  søk I ephorte  på  forhånd  basert  på  kontrakt.sap\_org\_id |
| Tittel                                                                          | SAP                                           | Blir  generert med følgende format:                                                               |                         |                                                                                                                                                   |
| {ansatte.etternavn}, {ansatte.fornavn } - {ansatteInfokontrakter.kontraktstype} | Tittel                                        | ansatteInfokontrakter.kontraktstype  transformeres  i  mikrotjeneste, se tabell «Kontraktstyper». |                         |                                                                                                                                                   |
| TittelOff                                                                       | SAP                                           | Samme som Tittel                                                                                  |                         |                                                                                                                                                   |

#### Journalpost

| Feltnavn                                                                                                                                                      | Kilde                           | Verdi                                                                                                                                                  | Feltnavn  i ephorte API | Kommentar                                                                                            |
|---------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------|------------------------------------------------------------------------------------------------------|
| Id                                                                                                                                                            | JournalEnhetId                  |                                                                                                                                                        |                         |                                                                                                      |     |
| Dokumenttype                                                                                                                                                  | Konfigurasjon  i  mikrotjeneste | UiB: I UiO: I                                                                                                                                          | DokumenttypeId:         |                                                                                                      |
| Dokument-kategori                                                                                                                                             | Konfigurasjon  i  mikrotjeneste | UiB: ND UiO:                                                                                                                                           | DokumentkategoriId:     | Ikke i bruk for UiO                                                                                  |
| Hjemmel                                                                                                                                                       | Konfigurasjon  i  mikrotjeneste | UiB: offl. § 26, 5. ledd UiO: offl § 13 jf  fvl § 13                                                                                                   | Hjemmel:                |                                                                                                      |
| Tilgangskode                                                                                                                                                  | Konfigurasjon  i  mikrotjeneste | UiB: P UiO: PB                                                                                                                                         | TilgangskodeId:         |                                                                                                      |
| Status                                                                                                                                                        | Konfigurasjon  i  mikrotjeneste | UiB: I UiO: J                                                                                                                                          |                         |                                                                                                      |
| JournalstatusId                                                                                                                                               |                                 |                                                                                                                                                        |                         |                                                                                                      |
| AdministrativEnhetId                                                                                                                                          | Ephorte                         | Hentet fra  søk                                                                                                                                        | AdministrativEnhetId    | Blir  utført  søk  på  forhånd  basert  på  infokontrakterFiler. organisasjonId                      |
| JournalEnhetId                                                                                                                                                | Ephorte                         | Hentet fra  søk                                                                                                                                        | JournalEnhetId          |                                                                                                      |
| Dokumentdato                                                                                                                                                  | SAP                             | infokontrakterFiler.dato                                                                                                                               | Dokumentdato            | Signeringsdato  hentet  fra  kontraktFiler  api                                                      |
| AvsenderMottaker                                                                                                                                              | SAP                             | ansatte.fornavn                                                                                                                                        | AvsenderMottaker        |                                                                                                      |
| Innholdsbeskrivelse                                                                                                                                           | SAP                             | Jp\_tittel  som  blir  generert I TOA-MS, følgende format:                                                                                             |                         |                                                                                                      |
| «Signert  arbeidsavtale ({kontraktstype}) - {ansatteinfokontrakter.organisasjonId} - { ansatteinfokontrakter .startdato}-{ ansatteinfokontrakter .sluttdato}» | Innholds-beskrivelse            | Kontrakttype  hentes  først  som  en  kode  i  ansatteinfokontrakter.kontraktstype  og  transformeres  i  mikrotjenesten. Se  tabell «Kontraktstyper». |                         |                                                                                                      |
| /ansatteinfokontrakter  gir  ut alle kontrakter  på  en  gitt  ansatt, så her bruker man tidligste  startdato  og  seneste  sluttdato.                        |                                 |                                                                                                                                                        |                         |                                                                                                      |
| Tilleggsattributt1                                                                                                                                            | SAP                             | ansatteinfokontrakter.sekvensnr                                                                                                                        | Tilleggsattributt1      | Felt som  blir  brukt for å lagre  sekvensnr, brukes for å sjekke om kontrakten er allerede  lagret. |

#### Dokument/dokumentversjon

| Feltnavn                       | Kilde                           | Verdi                          | Feltnavn  i ephorte API                 | Kommentar                                            |
|--------------------------------|---------------------------------|--------------------------------|-----------------------------------------|------------------------------------------------------|
| Filnavn                        | TOA-MS, SAP                     |                                | EiS.upload\_headers:file\_name          | Filnavnet  blir  satt  samme  verdi  som  jp\_tittel |
| Content                        | SAP                             | infokontrakterFiler.filinnhold | EiS.upload\_content:content             | Base64 decoded streng  fra  kontraktFiler  api       |
| TilgangskodeId                 | Konfigurasjon  i  mikrotjeneste | PB                             | DokumentBeskrivelse.TilgangskodeId  og  |                                                      |
| DokumentVersjon.TilgangskodeId |                                 |                                |                                         |                                                      |
| Dokumenttittel                 | TOA-MS                          | Journalpost.tittel             | DokumentBeskrivelse.Dokumenttittel      | Blir det samme  som  Journalpost  tittel             |
| DokumentkategoriId             | Konfigurasjon  i  mikrotjeneste | E                              | DokumentBeskrivelse. DokumentkategoriId |                                                      |
| Papirdokument                  | Hardkodet  i  mikrotjeneste     | “false”                        | DokumentVersjon. Papirdokument          | Papirdokument                                        |
| LagringsformatId               | Konfigurasjon  i  mikrotjeneste | RA-PDF                         | DokumentVersjon.LagringsformatId        |                                                      |
| TilknyttningHoved              | Konfigurasjon  i  mikrotjeneste | H                              | DokmentReferanse. TilknytningskodeId    |                                                      |

#### Kontraktstyper (støttetabell)

| Feltnavn | Kilde       | Verdi                                                           | Feltnavn  i ephorte    | Kommentar |
|----------|-------------|-----------------------------------------------------------------|------------------------|-----------|
| T1       | TOA-MS, SAP | Konfigurerbart: navn: "Timekontrakt" ordnings\_verdi: "212.3"   | Jornalpost.beskrivelse |           |
| T2       | TOA-MS, SAP | Konfigurerbart: navn: "Oppdragskontrakt"ordnings_verdi: "233"   | Jornalpost.beskrivelse |           |
| T3       | TOA-MS, SAP | Konfigurerbart  navn: "Månedskontrakt" ordnings\_verdi: "212.3" | Jornalpost.beskrivelse |           |

Feltet fra SAP, kontrakt.type blir brukt for å finne mappingen T1,T2,T3 

[1] Historiske kontrakter: Er kontrakter som er lagret mer en en mnd før dagens dato \
[2] [Uio ephorte konfigurasjon](https://git.app.uib.no/it-bott-integrasjoner/toa-ms-chart/-/blob/master/values/toa-uio-prod/values.yaml#L17) \
[3] [Uib ephorte konfigurasjon](https://git.app.uib.no/it-bott-integrasjoner/toa-ms-chart/-/blob/master/values/toa-uib-prod/values.yaml#L15) \
[4] [Uit ephorte konfigurasjon](https://git.app.uib.no/it-bott-integrasjoner/toa-ms-chart/-/blob/master/values/toa-uit-prod/values.yaml#L17) \
[5] [Feltbeskrivelser: TOA-MS -> Ephorte](https://uio.sharepoint.com/:w:/r/sites/BOTTintegrasjonsutviklingsteam/Delte%20dokumenter/General/TOA/Feltmapping_TOAMS_Ephorte.docx?d=wdc5a210c73084806b812149926a9160e&csf=1&web=1&e=lAdWHF)~~
