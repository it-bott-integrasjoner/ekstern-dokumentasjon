# Dokumenttjenesten

Dokumenttjenesten er en mikrotjeneste som tilgjengeliggjør dokumenter (fakturaer, kvitteringer, og annet) i Unit4 ERP for økonomipersonell i deres nettlesere.\
Økonomipersonell navigerer typisk til dokumenttjenesten (der de får se et dokument) ved å trykke en lenke i Tableau.

Tableau er et statistikk- og rapportverktøy (business intelligence system) eksempelvis for visualisering av diverse økonomidata.

Dokumenttjenesten er i dag i bruk av UiB, UiO og UiT.

Dokumenttjenesten henter gamle økonomidata fra en Oracledatabase hos UiO og eksponerer følgende endepunkter:

- /health - endepunkt for å sjekke om tjenesten er tilgjengelig
- /document - endepunkt for uthenting av dokumenter fra SAP\
  Autentisering gjennomføres basert på innsending av et token og ID på det forespurte dokumentet.
- /gamle-okonomidata/oa - endepunkt for uthenting av arkivfiler med økonomidata, som blir hentet fra Oracle Applications.
- /gamle-okonomidata/ss - endepunkt for uthenting av arkivfiler med økonomidata, som blir hentet fra StreamServe.

Autentisering gjennomføres basert på innsending av et token og ID på det forespurte dokumentet.

Dokumenttjenesten bruker følgende API-er:

    SAP (Regnskapsystem):
        dokumenter:
            GetDocumentRevision

## Kontaktpunkter

### BOTT-INT Drift

- RT-kø: bott-int-drift@usit.uio.no

Drifterne av bott-ints tjenester. Alle henvendelser sendes til dem.  
Drift sender henvendelser videre til utviklerene av bott-ints tjenester ved feil i tjenesten og ved endringsønsker.

### BOTT-INT Utvikling

- RT-kø: bott-int@bott-samarbeidet.no

Utviklerene av bott-ints tjenester.

### SAP

- [Seksjon for applikasjonsforvaltning](https://www.uio.no/om/organisasjon/los/ova/saf/index.html)
